package com.nashtech.trainning.services;

import org.springframework.lang.Nullable;

import javax.validation.constraints.NotBlank;

public interface CommonService {

    /**
     * Save or update data in cache with map name and key.
     *
     * @param mapName : map name cache
     * @param data    : data
     * @param key:    key value
     */
    <T> void saveOrUpdateCache(@NotBlank String mapName, @NotBlank String key, @Nullable T data);

    /**
     * Get data from cache with mapName and key.
     *
     * @param mapName : map name cache
     * @param key     : key value
     * @return data
     */
    @Nullable
    <T> T getDataFromCache(@NotBlank String mapName, @NotBlank String key);

    /**
     * Remove value from cache with mapName and key.
     *
     * @param mapName : map name cache
     * @param key     : key value
     */
    void removeValueFromCache(@NotBlank String mapName, @NotBlank String key);
}
