package com.nashtech.trainning.services;

import com.nashtech.trainning.dtos.ETagDTO;
import com.nashtech.trainning.dtos.ShapeCategoryDTO;
import com.nashtech.trainning.dtos.ShapeDTO;
import com.nashtech.trainning.exceptions.ObjectNotFoundException;
import org.springframework.security.core.userdetails.User;

import java.util.List;

public interface ShapeService {

    ShapeCategoryDTO saveCategory(ShapeCategoryDTO shapeCategory);

    ETagDTO<List<ShapeCategoryDTO>> getAllCategory(User user, String eTagValue);

    ShapeCategoryDTO getCategory(long id) throws ObjectNotFoundException;

    ETagDTO<ShapeCategoryDTO> getCategoryWithETag(long id) throws ObjectNotFoundException;

    void deleteCategory(long id) throws ObjectNotFoundException;

    List<ShapeDTO> getAllShapes();

    ETagDTO<List<ShapeDTO>> getAllShapesWithETag();

    ShapeDTO saveShape(ShapeDTO shape, User user);

    ShapeDTO getShapeById(long id) throws ObjectNotFoundException;

    ETagDTO<ShapeDTO> getShapeByIdWithETag(long id) throws ObjectNotFoundException;

    void deleteShape(long id) throws ObjectNotFoundException;
}
