package com.nashtech.trainning.services.impl;

import com.hazelcast.core.HazelcastInstance;
import com.nashtech.trainning.commons.Constants;
import com.nashtech.trainning.services.CommonService;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.StringUtils;
import org.springframework.stereotype.Service;
import org.springframework.util.Assert;

@Slf4j
@Service("commonService")
public class CommonServiceImpl implements CommonService {

    private final HazelcastInstance hazelcastInstance;

    public CommonServiceImpl(HazelcastInstance hazelcastInstance) {
        this.hazelcastInstance = hazelcastInstance;
    }

    @Override
    public <T> void saveOrUpdateCache(String mapName, String key, T data) {
        Assert.isTrue(StringUtils.isNotBlank(mapName), Constants.MAP_NAME_NOT_BLANK);
        Assert.isTrue(StringUtils.isNotBlank(key), Constants.KEY_NOT_BLANK);
        hazelcastInstance.getMap(mapName).put(key, data);
    }

    @Override
    public <T> T getDataFromCache(String mapName, String key) {
        Assert.isTrue(StringUtils.isNotBlank(mapName), Constants.MAP_NAME_NOT_BLANK);
        Assert.isTrue(StringUtils.isNotBlank(key), Constants.KEY_NOT_BLANK);
        return (T) hazelcastInstance.getMap(mapName).get(key);
    }

    @Override
    public void removeValueFromCache(String mapName, String key) {
        Assert.isTrue(StringUtils.isNotBlank(mapName), Constants.MAP_NAME_NOT_BLANK);
        Assert.isTrue(StringUtils.isNotBlank(key), Constants.KEY_NOT_BLANK);
        hazelcastInstance.getMap(mapName).remove(key);
    }

}
