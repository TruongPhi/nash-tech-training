package com.nashtech.trainning.services.impl;

import com.nashtech.trainning.dtos.ETagDTO;
import com.nashtech.trainning.dtos.ShapeCategoryDTO;
import com.nashtech.trainning.dtos.ShapeDTO;
import com.nashtech.trainning.entities.Shape;
import com.nashtech.trainning.entities.ShapeCategory;
import com.nashtech.trainning.exceptions.ObjectNotFoundException;
import com.nashtech.trainning.mappers.ShapeCategoryMapper;
import com.nashtech.trainning.mappers.ShapeMapper;
import com.nashtech.trainning.repositories.ShapeCategoryRepository;
import com.nashtech.trainning.repositories.ShapeRepository;
import com.nashtech.trainning.services.CommonService;
import com.nashtech.trainning.services.ShapeService;
import com.nashtech.trainning.utils.CommonUtils;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.security.core.userdetails.User;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.stream.Collectors;

@Service("ShapeService")
public class ShapeServiceImpl implements ShapeService {

    private final CommonService commonService;

    private final ShapeCategoryRepository shapeCategoryRepository;

    private final ShapeRepository shapeRepository;

    @Value("${spring.hazelcast.etag-map-name}")
    private String eTagMapName;

    @Value("${spring.hazelcast.data-map-name}")
    private String eTagDataName;

    public ShapeServiceImpl(CommonService commonService, ShapeCategoryRepository shapeCategoryRepository,
                            ShapeRepository shapeRepository) {
        this.shapeCategoryRepository = shapeCategoryRepository;
        this.shapeRepository = shapeRepository;
        this.commonService = commonService;
    }

    @Override
    public ShapeCategoryDTO saveCategory(ShapeCategoryDTO shapeCategory) {
        ShapeCategory entity = ShapeCategoryMapper.INSTANCE.toEntity(shapeCategory);
        shapeCategoryRepository.saveAndFlush(entity);
        shapeCategory.setId(entity.getId());

        return shapeCategory;
    }

    @Override
    public ETagDTO<List<ShapeCategoryDTO>> getAllCategory(User user, String eTagValue) {
        String keyETagCache = "shape_categories_" + user.getUsername();
        List<ShapeCategoryDTO> shapeCategories = null;
        if (StringUtils.isNotBlank(eTagValue)) {
            String eTagValueCache = commonService.getDataFromCache(eTagMapName, keyETagCache);
            if (eTagValue.equals(eTagValueCache)) {
                shapeCategories = commonService.getDataFromCache(eTagDataName, eTagValue);
            }
        }

        if (null == shapeCategories) {
            shapeCategories = shapeCategoryRepository.findAll()
                    .stream()
                    .map(ShapeCategoryMapper.INSTANCE::toDTO)
                    .collect(Collectors.toList());

            eTagValue = CommonUtils.calculateEtag(shapeCategories.stream()
                    .map(shapeCategory -> shapeCategory.getVersion().toString())
                    .collect(Collectors.joining("_")));

            commonService.saveOrUpdateCache(eTagMapName, keyETagCache, eTagValue);
            commonService.saveOrUpdateCache(eTagDataName, eTagValue, shapeCategories);
        }

        return new ETagDTO<>(eTagValue, shapeCategories);
    }

    @Override
    public ETagDTO<ShapeCategoryDTO> getCategoryWithETag(long id) throws ObjectNotFoundException {
        ShapeCategoryDTO shapeCategory = getCategory(id);
        return new ETagDTO<>(CommonUtils.calculateEtag(shapeCategory.getVersion().toString()), shapeCategory);
    }

    @Override
    public ShapeCategoryDTO getCategory(long id) throws ObjectNotFoundException {
        return shapeCategoryRepository.findOneById(id)
                .map(ShapeCategoryMapper.INSTANCE::toDTO)
                .orElseThrow(() -> new ObjectNotFoundException("Shape category not found"));
    }

    @Override
    public void deleteCategory(long id) throws ObjectNotFoundException {
        ShapeCategoryDTO shapeCategory = getCategory(id);
        shapeCategoryRepository.deleteById(shapeCategory.getId());
    }

    @Override
    public List<ShapeDTO> getAllShapes() {
        return shapeRepository.findAll()
                .stream()
                .map(ShapeMapper.INSTANCE::toDTO)
                .collect(Collectors.toList());
    }

    @Override
    public ETagDTO<List<ShapeDTO>> getAllShapesWithETag() {
        List<ShapeDTO> shapes = getAllShapes();
        String eTagValue = CommonUtils.calculateEtag(shapes.stream()
                .map(shapeCategory -> shapeCategory.getVersion().toString())
                .collect(Collectors.joining("_")));

        return new ETagDTO<>(eTagValue, shapes);
    }

    @Override
    public ShapeDTO saveShape(ShapeDTO shape, User user) {
        Shape entity = ShapeMapper.INSTANCE.toEntity(shape);
        ShapeCategory shapeCategory = shapeCategoryRepository.getOne(shape.getCatId());

        entity.setCategory(shapeCategory);
        entity.setUser(user.getUsername());
        shapeRepository.saveAndFlush(entity);
        shape.setId(entity.getId());

        return shape;
    }

    @Override
    public ShapeDTO getShapeById(long id) throws ObjectNotFoundException {
        return shapeRepository.findOneById(id)
                .map(ShapeMapper.INSTANCE::toDTO)
                .orElseThrow(() -> new ObjectNotFoundException("Shape not found"));
    }

    @Override
    public ETagDTO<ShapeDTO> getShapeByIdWithETag(long id) throws ObjectNotFoundException {
        ShapeDTO shape = getShapeById(id);
        return new ETagDTO<>(CommonUtils.calculateEtag(shape.getVersion().toString()), shape);
    }

    @Override
    public void deleteShape(long id) throws ObjectNotFoundException {
        ShapeDTO shape = getShapeById(id);
        shapeRepository.deleteById(shape.getId());
    }
}
