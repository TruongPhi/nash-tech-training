package com.nashtech.trainning.configurations;

import com.hazelcast.config.*;
import com.hazelcast.core.Hazelcast;
import com.hazelcast.core.HazelcastInstance;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Primary;
import org.springframework.core.env.Environment;

@Configuration
public class HazelcastConfiguration {

    private final Environment env;

    public HazelcastConfiguration(Environment env) {
        this.env = env;
    }

    @Bean
    public Config hazelCastConfig() {
        Config config = new Config();
        config.setInstanceName(env.getProperty("spring.hazelcast.instance"));

        GroupConfig groupConfig = config.getGroupConfig();
        groupConfig.setName(env.getProperty("spring.hazelcast.group.name", "spring-shape"));

        NetworkConfig networkConfig = config.getNetworkConfig();
        networkConfig.setPort(5731);
        networkConfig.setPortAutoIncrement(true);
        MulticastConfig multicastConfig = networkConfig.getJoin().getMulticastConfig();
        multicastConfig.setEnabled(env.getProperty("spring.hazelcast.multicast", Boolean.class, false));
        multicastConfig.setMulticastGroup("224.2.2.3");
        multicastConfig.setMulticastPort(54327);

        MapConfig mapConfig = new MapConfig();
        mapConfig.setName(env.getProperty("spring.hazelcast.etag-map-name"));
        mapConfig.getMaxSizeConfig().setSize(env.getProperty("spring.hazelcast.etag.max-size", Integer.class, 1024));
        mapConfig.setReadBackupData(env.getProperty("spring.hazelcast.etag.backup", Boolean.class, false));
        mapConfig.setBackupCount(env.getProperty("spring.hazelcast.etag.backup-count", Integer.class, 1));
        mapConfig.setTimeToLiveSeconds(env.getProperty("spring.hazelcast.time-to-live-seconds", Integer.class, 900));
        mapConfig.setEvictionPolicy(EvictionPolicy.LRU);

        MapConfig mapPdfConfig = new MapConfig();
        mapPdfConfig.setName(env.getProperty("spring.hazelcast.data-map-name"));
        mapPdfConfig.getMaxSizeConfig().setSize(env.getProperty("spring.hazelcast.data.max-size", Integer.class, 1024));
        mapPdfConfig.setReadBackupData(env.getProperty("spring.hazelcast.data.backup", Boolean.class, false));
        mapPdfConfig.setBackupCount(env.getProperty("spring.hazelcast.data.backup-count", Integer.class, 1));
        mapPdfConfig.setTimeToLiveSeconds(env.getProperty("spring.hazelcast.time-to-live-seconds", Integer.class, 900));
        mapPdfConfig.setEvictionPolicy(EvictionPolicy.LRU);

        config.addMapConfig(mapConfig);
        config.addMapConfig(mapPdfConfig);
        return config;
    }

    @Primary
    @Bean
    public HazelcastInstance hazelcastInstance() {
        return Hazelcast.newHazelcastInstance(hazelCastConfig());
    }

}
