package com.nashtech.trainning.configurations;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Configuration;
import org.springframework.core.env.Environment;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.oauth2.config.annotation.web.configuration.EnableResourceServer;
import org.springframework.security.oauth2.config.annotation.web.configuration.ResourceServerConfigurerAdapter;
import org.springframework.security.oauth2.config.annotation.web.configurers.ResourceServerSecurityConfigurer;
import org.springframework.security.oauth2.provider.token.ResourceServerTokenServices;

@Configuration
@EnableResourceServer
public class OAuth2ResourceServerConfiguration extends ResourceServerConfigurerAdapter {

    private final ResourceServerTokenServices tokenServices;

    private final Environment env;

    @Autowired
    public OAuth2ResourceServerConfiguration(ResourceServerTokenServices tokenServices, Environment env) {
        this.tokenServices = tokenServices;
        this.env = env;
    }

    @Override
    public void configure(ResourceServerSecurityConfigurer resources) {
        resources.tokenServices(tokenServices).resourceId(env.getProperty("spring.oauth.resourceIds"));
    }

    @Override
    public void configure(HttpSecurity http) throws Exception {
        http.cors()
                .and().requestMatchers().and().authorizeRequests()
                .antMatchers("/v3/api-docs/**",
                        "/swagger-resources/configuration/ui",
                        "/swagger-resources",
                        "/swagger-resources/configuration/security",
                        "/swagger-ui.html").permitAll()
                .antMatchers("/api/**").authenticated();
    }

}
