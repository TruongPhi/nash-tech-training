package com.nashtech.trainning.configurations;

import com.nashtech.trainning.dtos.ShapeCategoryDTO;
import com.nashtech.trainning.entities.OauthClientDetail;
import com.nashtech.trainning.entities.Role;
import com.nashtech.trainning.entities.User;
import com.nashtech.trainning.exceptions.ObjectNotFoundException;
import com.nashtech.trainning.repositories.OauthClientDetailRepository;
import com.nashtech.trainning.repositories.RoleRepository;
import com.nashtech.trainning.repositories.UserRepository;
import com.nashtech.trainning.services.ShapeService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.ApplicationArguments;
import org.springframework.boot.ApplicationRunner;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Component;

import java.util.Arrays;
import java.util.Collections;
import java.util.HashSet;
import java.util.Set;

@Component
@Slf4j
public class DataGenerator implements ApplicationRunner {

    private final ShapeService shapeService;

    private final UserRepository userRepository;

    private final RoleRepository roleRepository;

    private final OauthClientDetailRepository oauthClientDetailRepository;

    private final PasswordEncoder passwordEncoder;

    @Value("${default.password}")
    private String defaultPassword;

    @Value("${spring.oauth.resourceIds}")
    private String resourceIds;

    @Value("${spring.tokenTimeout:86400}")
    private Integer accessTokenValiditySecond;

    @Value("${spring.tokenRefresh:172800}")
    private Integer refreshTokenValiditySecond;

    @Value("${spring.login.clientid}")
    private String clientId;

    @Value("${spring.login.client_secret}")
    private String clientSecret;

    @Value("${spring.login.grantTypes}")
    private String grantTypes;

    @Value("${spring.login.scope}")
    private String scope;


    @Autowired
    public DataGenerator(ShapeService shapeService, UserRepository userRepository, RoleRepository roleRepository,
                         OauthClientDetailRepository oauthClientDetailRepository, PasswordEncoder passwordEncoder) {
        this.shapeService = shapeService;
        this.userRepository = userRepository;
        this.roleRepository = roleRepository;
        this.oauthClientDetailRepository = oauthClientDetailRepository;
        this.passwordEncoder = passwordEncoder;
    }

    @Override
    public void run(ApplicationArguments args) throws Exception {
        log.info("------Generate data-----");

        initUsers();
        initAccountServices();
        initShapeCategories();

        log.info("------End generate data-----");
    }

    private void initUsers() throws ObjectNotFoundException {
        Role adminRole = roleRepository.findByNameIgnoreCase("admin")
                .orElseThrow(() -> new ObjectNotFoundException("Admin role not found"));
        Role userRole = roleRepository.findByNameIgnoreCase("user")
                .orElseThrow(() -> new ObjectNotFoundException("User role not found"));

        createUser("admin", defaultPassword, adminRole);
        createUser("user", defaultPassword, userRole);
    }

    public User createUser(String username, String password, Role role) {
        User user = new User();
        user.setUsername(username);
        user.setPassword(passwordEncoder.encode(password));
        user.setRole(role);

        return userRepository.saveAndFlush(user);
    }

    private void initAccountServices() {
        createAccountService(clientId, clientSecret, resourceIds, scope, grantTypes, null,
                accessTokenValiditySecond, refreshTokenValiditySecond);
    }

    private OauthClientDetail createAccountService(String clientId, String clientSecret, String resourceIds, String scope,
                                                   String grantTypes, String redirectUri, Integer accessTokenValiditySeconds,
                                                   Integer refreshTokenValiditySeconds) {
        OauthClientDetail oauthClientDetail = new OauthClientDetail();
        oauthClientDetail.setClientId(clientId);
        oauthClientDetail.setClientSecret(passwordEncoder.encode(clientSecret));
        oauthClientDetail.setResourceIds(resourceIds);
        oauthClientDetail.setScope(scope);
        oauthClientDetail.setAuthorizedGrantTypes(grantTypes);
        oauthClientDetail.setWebServerRedirectUri(redirectUri);
        oauthClientDetail.setAccessTokenValidity(accessTokenValiditySeconds);
        oauthClientDetail.setRefreshTokenValidity(refreshTokenValiditySeconds);

        return oauthClientDetailRepository.saveAndFlush(oauthClientDetail);
    }

    private void initShapeCategories() {
        createShapeCategory("TRIANGLE",
                "(sqrt( (a+b+c) * (a+b-c) * (b+c-a) * (c+a-b)))/4",
                new HashSet<>(Arrays.asList("a", "b", "c")));

        createShapeCategory("SQUARE",
                "a * a",
                new HashSet<>(Collections.singletonList("a")));

        createShapeCategory("RECTANGLE",
                "l * w",
                new HashSet<>(Arrays.asList("l", "w")));

        createShapeCategory("PARALLELOGRAM",
                "a * h",
                new HashSet<>(Arrays.asList("a", "h")));

        createShapeCategory("CIRCLE",
                "r * r * 3.14",
                new HashSet<>(Collections.singletonList("r")));
    }

    private ShapeCategoryDTO createShapeCategory(String shapeCategoryName, String areaFormula,
                                                 Set<String> shapeAttributes) {
        ShapeCategoryDTO shapeCategory = new ShapeCategoryDTO();
        shapeCategory.setName(shapeCategoryName);
        shapeCategory.setAreaFormula(areaFormula);
        shapeCategory.setAttributes(shapeAttributes);

        return this.shapeService.saveCategory(shapeCategory);
    }

}
