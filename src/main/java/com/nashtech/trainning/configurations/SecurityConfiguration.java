package com.nashtech.trainning.configurations;

import com.nashtech.trainning.commons.CustomAccessTokenConverter;
import com.nashtech.trainning.utils.CommonUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.core.env.Environment;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.config.annotation.method.configuration.EnableGlobalMethodSecurity;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.config.http.SessionCreationPolicy;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.security.oauth2.provider.ClientDetailsService;
import org.springframework.security.oauth2.provider.token.DefaultAccessTokenConverter;
import org.springframework.security.oauth2.provider.token.DefaultTokenServices;
import org.springframework.security.oauth2.provider.token.DefaultUserAuthenticationConverter;
import org.springframework.security.oauth2.provider.token.TokenStore;
import org.springframework.security.oauth2.provider.token.store.JwtAccessTokenConverter;
import org.springframework.security.oauth2.provider.token.store.JwtTokenStore;
import org.springframework.web.cors.CorsConfiguration;
import org.springframework.web.cors.CorsConfigurationSource;
import org.springframework.web.cors.UrlBasedCorsConfigurationSource;

@Configuration
@EnableWebSecurity
@EnableGlobalMethodSecurity(prePostEnabled = true)
public class SecurityConfiguration extends WebSecurityConfigurerAdapter {

    private final Environment env;

    private final UserDetailsService userDetailsService;

    private final ClientDetailsService clientDetailsService;

    private final PasswordEncoder passwordEncoder;

    @Autowired
    public SecurityConfiguration(Environment env, UserDetailsService userDetailsService, PasswordEncoder passwordEncoder,
                                 ClientDetailsService clientDetailsService) {
        this.env = env;
        this.userDetailsService = userDetailsService;
        this.passwordEncoder = passwordEncoder;
        this.clientDetailsService = clientDetailsService;
    }

    @Bean
    @Override
    protected AuthenticationManager authenticationManager() throws Exception {
        return super.authenticationManager();
    }

    @Override
    protected void configure(AuthenticationManagerBuilder auth) throws Exception {
        auth.userDetailsService(userDetailsService).passwordEncoder(passwordEncoder);
    }

    @Override
    protected void configure(HttpSecurity http) throws Exception {
        http.formLogin().disable()
                .sessionManagement().sessionCreationPolicy(SessionCreationPolicy.STATELESS)
                .and().csrf().disable();
    }

    @Bean
    public TokenStore tokenStore() {
        return new JwtTokenStore(accessTokenConverter());
    }

    @Bean("tokenServices")
    public DefaultTokenServices tokenServices() throws Exception {
        DefaultTokenServices tokenServices = new DefaultTokenServices();
        tokenServices.setTokenStore(tokenStore());
        tokenServices.setTokenEnhancer(accessTokenConverter());
        tokenServices.setClientDetailsService(clientDetailsService);
        tokenServices.setAuthenticationManager(authenticationManager());
        tokenServices.setAccessTokenValiditySeconds(
                env.getProperty("spring.oauth.tokenTimeout", Integer.class, 86400));
        tokenServices.setSupportRefreshToken(true);
        tokenServices.setRefreshTokenValiditySeconds(
                env.getProperty("spring.oauth.tokenRefresh", Integer.class, 172800));

        return tokenServices;
    }

    @Bean
    public JwtAccessTokenConverter accessTokenConverter() {
        CustomAccessTokenConverter converter = new CustomAccessTokenConverter();

        DefaultUserAuthenticationConverter userTokenConverter = new DefaultUserAuthenticationConverter();
        userTokenConverter.setUserDetailsService(userDetailsService);
        DefaultAccessTokenConverter accessTokenConverter = new DefaultAccessTokenConverter();
        accessTokenConverter.setUserTokenConverter(userTokenConverter);

        converter.setSigningKey(env.getProperty("spring.sign.key", "spring"));
        converter.setAccessTokenConverter(accessTokenConverter);

        return converter;
    }


    @Bean
    public CorsConfigurationSource corsConfigurationSource() {
        CorsConfiguration configuration = new CorsConfiguration();
        configuration.setAllowedOrigins(
                CommonUtils.spiltStringToList(env.getProperty("spring.allowed-origins", "*"), ",")
        );
        configuration.setAllowCredentials(env.getProperty("spring.allow-credentials", Boolean.class, false));
        configuration.setAllowedMethods(
                CommonUtils.spiltStringToList(env.getProperty("spring.allowed-methods", ""), ",")
        );
        configuration.setMaxAge(env.getProperty("spring.max-age", Long.class, 3600L));
        configuration.setAllowedHeaders(
                CommonUtils.spiltStringToList(env.getProperty("spring.allowed-headers", ""), ",")
        );

        UrlBasedCorsConfigurationSource source = new UrlBasedCorsConfigurationSource();
        source.registerCorsConfiguration("/**", configuration);

        return source;
    }
}
