package com.nashtech.trainning.configurations;

import com.nashtech.trainning.utils.CommonUtils;
import io.swagger.v3.oas.models.Components;
import io.swagger.v3.oas.models.OpenAPI;
import io.swagger.v3.oas.models.info.Info;
import io.swagger.v3.oas.models.security.OAuthFlow;
import io.swagger.v3.oas.models.security.OAuthFlows;
import io.swagger.v3.oas.models.security.Scopes;
import io.swagger.v3.oas.models.security.SecurityScheme;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import java.util.List;

@Configuration
public class SwaggerConfiguration {

    @Value("${module-name}")
    private String moduleName;

    @Value("${api-version}")
    private String apiVersion;

    @Value("${spring.login.scope}")
    private String scopes;

    @Value("${spring.host}")
    private String host;

    @Bean
    public OpenAPI openAPI() {
        final String securitySchemeName = "security_auth";

        return new OpenAPI()
                .components(
                        new Components()
                                .addSecuritySchemes(securitySchemeName,
                                        createSecuritySchemeOauth2(securitySchemeName)
                                )
                )
//                .addSecurityItem(new SecurityRequirement().addList(securitySchemeName))
                .info(createInfo());
    }

    private SecurityScheme createSecuritySchemeOauth2(String securitySchemeName) {
        return new SecurityScheme()
                .name(securitySchemeName)
                .description("This API uses OAuth 2. [More info](https://api.example.com/docs/auth)")
                .type(SecurityScheme.Type.OAUTH2)
                .flows(new OAuthFlows()
                        .password(createFlowPassword())
                );
    }

    private OAuthFlow createFlowPassword() {
        OAuthFlow oAuthFlow = new OAuthFlow()
                .tokenUrl(host + "/oauth/token")
                .refreshUrl(host + "/oauth/token");

        List<String> scopesAuth = CommonUtils.spiltStringToList(scopes, ",");
        for (String scope : scopesAuth) {
            oAuthFlow.scopes(new Scopes().addString(scope, scope));
        }

        return oAuthFlow;
    }

    private Info createInfo() {
        final String apiTitle = String.format("%s API", StringUtils.capitalize(moduleName));
        final String apiDescription = String.format("This app provides REST APIs for %s",
                StringUtils.capitalize(moduleName));

        return new Info()
                .title(apiTitle)
                .version(apiVersion)
                .description(apiDescription);
    }

}
