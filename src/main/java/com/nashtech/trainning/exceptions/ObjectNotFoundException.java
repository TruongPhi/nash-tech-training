package com.nashtech.trainning.exceptions;

import com.nashtech.trainning.commons.ErrorCode;

public class ObjectNotFoundException extends ErrorCodeException {

    private static final long serialVersionUID = 947440631498154276L;

    public ObjectNotFoundException(String message) {
        super(ErrorCode.OBJECT_NOT_FOUND,  message);
    }

    public ObjectNotFoundException(String errorCode, String message) {
        super(errorCode, message);
    }

    public ObjectNotFoundException(String message, Throwable cause) {
        super(ErrorCode.OBJECT_NOT_FOUND.value, message, cause);
    }
}
