package com.nashtech.trainning.exceptions;

import com.nashtech.trainning.commons.ErrorCode;

public class ErrorCodeException extends Exception {

    private static final long serialVersionUID = -2004340163726702628L;

    private ErrorCode errorCode;

    public ErrorCodeException(ErrorCode errorCode, String message) {
        super(message);
        this.errorCode = errorCode;
    }
    public ErrorCodeException(String errorCode, String message) {
        super(message);
        this.errorCode = ErrorCode.fromValue(errorCode);
    }

    public ErrorCodeException(String errorCode, String message, Throwable cause) {
        super(message, cause);
        this.errorCode = ErrorCode.fromValue(errorCode);
    }

    public ErrorCode getErrorCode() {
        return errorCode;
    }
}
