package com.nashtech.trainning.exceptions;

public class CalcAreaException extends Exception {

    private static final long serialVersionUID = -4740235548887096570L;

    public CalcAreaException(String message) {
        super(message);
    }

    public CalcAreaException(String message, Throwable cause) {
        super(message, cause);
    }
}
