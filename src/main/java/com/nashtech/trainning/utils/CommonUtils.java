package com.nashtech.trainning.utils;

import org.apache.commons.codec.digest.DigestUtils;

import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;

public class CommonUtils {

    private CommonUtils() {
        throw new IllegalStateException("CommonUtilsUtility class");
    }

    public static List<String> spiltStringToList(String string, String sign) {
        String regex = String.format("\\s*%s\\s*", sign);

        return Arrays.stream(string
                .split(regex))
                .collect(Collectors.toList());
    }

    public static String calculateEtag(String id) {
        return DigestUtils.md5Hex(id);
    }
}
