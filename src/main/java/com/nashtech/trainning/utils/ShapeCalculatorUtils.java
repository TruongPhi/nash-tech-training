package com.nashtech.trainning.utils;

import com.nashtech.trainning.exceptions.CalcAreaException;
import org.cheffo.jeplite.JEP;
import org.cheffo.jeplite.ParseException;

import java.util.Map;

public class ShapeCalculatorUtils {

    private ShapeCalculatorUtils() {
        throw new IllegalStateException("ShapeCalculatorUtility class");
    }

    public static double calcArea(String formula, Map<String, Double> values) throws CalcAreaException {
        JEP jep = new JEP();
        jep.addStandardFunctions();
        for (Map.Entry<String, Double> me : values.entrySet()) {
            jep.addVariable(me.getKey(), me.getValue());
        }
        try {
            jep.parseExpression(formula);
            return jep.getValue();
        } catch (ParseException ex) {
            throw new CalcAreaException(ex.getMessage());
        }

    }
}
