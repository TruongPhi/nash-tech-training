package com.nashtech.trainning.mappers;

import com.nashtech.trainning.dtos.ShapeCategoryDTO;
import com.nashtech.trainning.entities.ShapeAttribute;
import com.nashtech.trainning.entities.ShapeCategory;

import java.util.stream.Collectors;

public class ShapeCategoryMapperDecorator implements ShapeCategoryMapper {

    private final ShapeCategoryMapper delegate;

    public ShapeCategoryMapperDecorator(ShapeCategoryMapper delegate) {
        this.delegate = delegate;
    }

    @Override
    public ShapeCategoryDTO toDTO(ShapeCategory shapeCategory) {
        ShapeCategoryDTO dto = delegate.toDTO(shapeCategory);
        dto.setAttributes(shapeCategory.getAttributes()
                .stream()
                .map(ShapeAttribute::getAttributeName)
                .collect(Collectors.toSet()));
        return dto;
    }

    @Override
    public ShapeCategory toEntity(ShapeCategoryDTO shapeCategoryDto) {
        ShapeCategory entity = delegate.toEntity(shapeCategoryDto);
        shapeCategoryDto.getAttributes().forEach(f -> {
            ShapeAttribute attribute = new ShapeAttribute();
            attribute.setAttributeName(f.toLowerCase());
            attribute.setCategory(entity);
            entity.getAttributes().add(attribute);
        });

        return entity;
    }
}
