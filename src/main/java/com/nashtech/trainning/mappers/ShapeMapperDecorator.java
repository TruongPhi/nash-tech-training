package com.nashtech.trainning.mappers;

import com.nashtech.trainning.dtos.ShapeDTO;
import com.nashtech.trainning.entities.Shape;
import com.nashtech.trainning.entities.ShapeAttributeValue;

public class ShapeMapperDecorator implements ShapeMapper {

    private final ShapeMapper delegate;

    public ShapeMapperDecorator(ShapeMapper delegate) {
        this.delegate = delegate;
    }

    @Override
    public ShapeDTO toDTO(Shape shape) {
        final ShapeDTO dto = delegate.toDTO(shape);
        shape.getAttributes().forEach(e -> {
            dto.getAttributes().put(e.getAttributeName(), e.getValue());
        });

        return dto;
    }

    @Override
    public Shape toEntity(ShapeDTO shapeDTO) {
        final Shape entity = delegate.toEntity(shapeDTO);
        shapeDTO.getAttributes().forEach((key, value) -> {
            ShapeAttributeValue attributeValue = new ShapeAttributeValue();
            attributeValue.setAttributeName(key);
            attributeValue.setValue(value);
            attributeValue.setShape(entity);
            entity.getAttributes().add(attributeValue);

        });

        return entity;
    }
}
