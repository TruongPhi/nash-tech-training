package com.nashtech.trainning.mappers;


import com.nashtech.trainning.dtos.ShapeDTO;
import com.nashtech.trainning.entities.Shape;
import org.mapstruct.DecoratedWith;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.mapstruct.factory.Mappers;

@Mapper
@DecoratedWith(ShapeMapperDecorator.class)
public interface ShapeMapper {

    ShapeMapper INSTANCE = Mappers.getMapper(ShapeMapper.class);

    @Mapping(target = "attributes", ignore = true)
    @Mapping(target = "catId", source = "category.id")
    @Mapping(target = "name", source = "category.name")
    ShapeDTO toDTO(Shape shape);

    @Mapping(target = "attributes", ignore = true)
    @Mapping(target = "user", ignore = true)
    @Mapping(target = "category", ignore = true)
    @Mapping(target = "attributesMap", ignore = true)
    Shape toEntity(ShapeDTO shapeDTO);
}
