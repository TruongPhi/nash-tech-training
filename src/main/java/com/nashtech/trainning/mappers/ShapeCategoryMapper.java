package com.nashtech.trainning.mappers;

import com.nashtech.trainning.dtos.ShapeCategoryDTO;
import com.nashtech.trainning.entities.ShapeCategory;
import org.mapstruct.DecoratedWith;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.mapstruct.factory.Mappers;

@Mapper
@DecoratedWith(ShapeCategoryMapperDecorator.class)
public interface ShapeCategoryMapper {

    ShapeCategoryMapper INSTANCE = Mappers.getMapper(ShapeCategoryMapper.class);

    @Mapping(target = "attributes", ignore = true)
    ShapeCategoryDTO toDTO(ShapeCategory shapeCategory);

    @Mapping(target = "attributes", ignore = true)
    @Mapping(target = "areaFormula", expression = "java(shapeCategoryDto.getAreaFormula().toLowerCase())")
    @Mapping(target = "name", expression = "java(shapeCategoryDto.getName().toUpperCase())")
    ShapeCategory toEntity(ShapeCategoryDTO shapeCategoryDto);
}
