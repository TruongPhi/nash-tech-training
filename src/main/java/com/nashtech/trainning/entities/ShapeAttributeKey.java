package com.nashtech.trainning.entities;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.io.Serializable;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class ShapeAttributeKey implements Serializable {

    private static final long serialVersionUID = -4819659764820017946L;

    private String attributeName;
    private long category;
}
