package com.nashtech.trainning.entities;

import lombok.Data;
import org.hibernate.validator.constraints.Length;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import java.io.Serializable;
import java.util.Objects;

@Data
@IdClass(ShapeAttributeKey.class)
@Entity(name = "shape_attributes")
public class ShapeAttribute implements Serializable {

    private static final long serialVersionUID = -7522586119569302610L;

    public ShapeAttribute() {

    }

    public ShapeAttribute(@NotNull @Size(min = 1, max = 5) String attributeName, ShapeCategory category) {
        this.attributeName = attributeName;
        this.category = category;
    }

    @Id
    @Column(name = "attribute_name", nullable = false)
    @Length(min = 1, max = 5)
    private String attributeName;

    @Id
    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(updatable = false, insertable = false)
    private ShapeCategory category;

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        ShapeAttribute that = (ShapeAttribute) o;
        return Objects.equals(attributeName, that.attributeName) &&
                Objects.equals(category, that.category);
    }

    @Override
    public int hashCode() {
        return Objects.hash(attributeName, category);
    }
}
