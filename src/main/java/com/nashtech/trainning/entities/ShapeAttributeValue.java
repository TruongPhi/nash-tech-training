package com.nashtech.trainning.entities;

import lombok.Data;

import javax.persistence.*;
import java.io.Serializable;
import java.util.Objects;

@Data
@IdClass(ShapeAttributeValueKey.class)
@Entity(name = "shape_attributes_values")
public class ShapeAttributeValue implements Serializable {

    private static final long serialVersionUID = 6737557923582138898L;

    @Id
    private String attributeName;

    private Double value;

    @Id
    @ManyToOne(fetch = FetchType.LAZY, optional = false)
    @JoinColumn(insertable = false, updatable = false)
    private Shape shape;


    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        ShapeAttributeValue that = (ShapeAttributeValue) o;
        return Objects.equals(attributeName, that.attributeName) &&
                Objects.equals(shape.getId(), that.shape.getId());
    }

    @Override
    public int hashCode() {
        return Objects.hash(attributeName, shape);
    }

}
