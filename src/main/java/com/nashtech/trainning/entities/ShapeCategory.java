package com.nashtech.trainning.entities;

import lombok.Data;
import lombok.ToString;
import org.hibernate.validator.constraints.Length;

import javax.persistence.*;
import java.io.Serializable;
import java.util.HashSet;
import java.util.Objects;
import java.util.Set;

@Data
@ToString(exclude = {"attributes"})
@Entity(name = "shape_categories")
public class ShapeCategory implements Serializable {

    private static final long serialVersionUID = -2544511265631782946L;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id", nullable = false)
    private Long id;

    @Column(name = "name", length = 50, unique = true, nullable = false)
    @Length(min = 1, max = 50)
    private String name;

    @Column(nullable = false)
    @Length(min = 1, max = 255)
    private String areaFormula;

    @Version
    private Long version;

    @OneToMany(
            mappedBy = "category",
            cascade = CascadeType.ALL,
            orphanRemoval = true
    )
    private Set<ShapeAttribute> attributes = new HashSet<>();

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        ShapeCategory that = (ShapeCategory) o;
        return Objects.equals(id, that.id);
    }

    @Override
    public int hashCode() {
        return Objects.hash(id);
    }
}
