package com.nashtech.trainning.entities;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.io.Serializable;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class ShapeAttributeValueKey implements Serializable {

    private static final long serialVersionUID = 7636505593289370803L;

    private String attributeName;

    private long shape;
}
