package com.nashtech.trainning.entities;

import lombok.Getter;
import lombok.Setter;

import javax.persistence.*;
import java.io.Serializable;
import java.util.*;

@Setter
@Getter
@Entity(name = "shapes")
public class Shape implements Serializable {

    private static final long serialVersionUID = 8602558447611262687L;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id", nullable = false)
    private Long id;

    @ManyToOne
    @JoinColumn(name = "cat_id", referencedColumnName = "id", nullable = false)
    private ShapeCategory category;

    @Column(name = "owner")
    private String user;

    @Version
    private Long version;

    @OneToMany(
            mappedBy = "shape",
            cascade = CascadeType.ALL,
            orphanRemoval = true
    )
    private Set<ShapeAttributeValue> attributes = new HashSet<>();


    public Map<String, Double> getAttributesMap() {
        Map<String, Double> map = new HashMap<>();
        attributes.forEach(attr ->
                map.put(attr.getAttributeName(), attr.getValue())
        );
        return map;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Shape shape = (Shape) o;
        return Objects.equals(id, shape.id);
    }

    @Override
    public int hashCode() {
        return Objects.hash(id);
    }

    @Override
    public String toString() {
        return "ShapeService{" +
                "id=" + id +
                ", category=" + category +
                ", user='" + user + '\'' +
                '}';
    }
}
