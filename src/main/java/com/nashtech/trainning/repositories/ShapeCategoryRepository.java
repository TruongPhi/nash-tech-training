package com.nashtech.trainning.repositories;

import com.nashtech.trainning.entities.ShapeCategory;
import org.springframework.data.jpa.repository.EntityGraph;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;
import java.util.Optional;

public interface ShapeCategoryRepository extends JpaRepository<ShapeCategory, Long> {

    @EntityGraph(attributePaths = "attributes")
    Optional<ShapeCategory> findOneById(Long id);

    @EntityGraph(attributePaths = "attributes")
    List<ShapeCategory> findAll();
}
