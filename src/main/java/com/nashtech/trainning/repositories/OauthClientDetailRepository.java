package com.nashtech.trainning.repositories;

import com.nashtech.trainning.entities.OauthClientDetail;
import org.springframework.data.jpa.repository.JpaRepository;

public interface OauthClientDetailRepository extends JpaRepository<OauthClientDetail, String> {
}
