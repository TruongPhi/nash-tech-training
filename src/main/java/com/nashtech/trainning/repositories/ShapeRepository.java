package com.nashtech.trainning.repositories;

import com.nashtech.trainning.entities.Shape;
import org.springframework.data.jpa.repository.EntityGraph;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;
import java.util.Optional;

public interface ShapeRepository extends JpaRepository<Shape, Long> {

    @EntityGraph(attributePaths = "attributes")
    List<Shape> findAllByUser(String user);

    @EntityGraph(attributePaths = {"category", "attributes"})
    Optional<Shape> findOneById(Long id);

    @EntityGraph(attributePaths = {"category", "attributes"})
    List<Shape> findAll();
}
