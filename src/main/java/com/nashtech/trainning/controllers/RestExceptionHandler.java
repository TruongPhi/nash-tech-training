package com.nashtech.trainning.controllers;

import com.nashtech.trainning.commons.ErrorCode;
import com.nashtech.trainning.dtos.ObjectError;
import com.nashtech.trainning.dtos.ResponseDTO;
import com.nashtech.trainning.exceptions.ObjectNotFoundException;
import org.springframework.core.Ordered;
import org.springframework.core.annotation.Order;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.servlet.mvc.method.annotation.ResponseEntityExceptionHandler;

@Order(Ordered.HIGHEST_PRECEDENCE)
@ControllerAdvice
public class RestExceptionHandler extends ResponseEntityExceptionHandler {

    /**
     * Method for handle all exception when no exception have catching.
     *
     * @param ex : exception
     * @return message error
     */
    @ExceptionHandler({Exception.class})
    @ResponseStatus(HttpStatus.BAD_REQUEST)
    public ResponseDTO<?> handleException(Exception ex) {
        logger.error(ex.getMessage(), ex);
        return new ResponseDTO<>(new ObjectError(ErrorCode.EXCEPTION));
    }

    /**
     * Method for handle all ObjectNotFoundException.
     *
     * @param ex : exception
     * @return message error
     */
    @ExceptionHandler({ObjectNotFoundException.class})
    @ResponseStatus(HttpStatus.NOT_FOUND)
    public ResponseDTO<?> handleObjectNotFound(ObjectNotFoundException ex) {
        return new ResponseDTO<>(new ObjectError(ex.getErrorCode().value, ex.getMessage()));
    }
}
