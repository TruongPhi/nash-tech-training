package com.nashtech.trainning.controllers;

import com.nashtech.trainning.dtos.ETagDTO;
import com.nashtech.trainning.dtos.ResponseDTO;
import com.nashtech.trainning.dtos.ShapeCategoryDTO;
import com.nashtech.trainning.dtos.ShapeDTO;
import com.nashtech.trainning.exceptions.CalcAreaException;
import com.nashtech.trainning.exceptions.ObjectNotFoundException;
import com.nashtech.trainning.services.ShapeService;
import com.nashtech.trainning.utils.ShapeCalculatorUtils;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.Parameter;
import io.swagger.v3.oas.annotations.enums.ParameterIn;
import io.swagger.v3.oas.annotations.security.SecurityRequirement;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.userdetails.User;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;


@Slf4j
@SecurityRequirement(name = "security_auth")
@RestController
@RequestMapping(value = "/api/shape", produces = MediaType.APPLICATION_JSON_VALUE)
public class ShapeController {

    private final ShapeService shapeService;

    @Autowired
    public ShapeController(ShapeService shapeService) {
        this.shapeService = shapeService;
    }

    @GetMapping("/categories")
    public ResponseDTO<List<ShapeCategoryDTO>> getAllCategory(
            @RequestHeader(name = "If-None-Match", required = false) String ifNoneMatchEtag,
            Authentication authentication) {
        User user = (User) authentication.getPrincipal();
        ETagDTO<List<ShapeCategoryDTO>> result = shapeService.getAllCategory(user, ifNoneMatchEtag);

        return new ResponseDTO<>(result.getETagValue(), result.getData());
    }

    @GetMapping("/category/{categoryId}")
    public ResponseDTO<ShapeCategoryDTO> getCategory(@PathVariable("categoryId") long id,
                                                     @RequestHeader(name = "If-None-Match", required = false) String ifNoneMatchEtag)
            throws ObjectNotFoundException {
        ETagDTO<ShapeCategoryDTO> result = shapeService.getCategoryWithETag(id);
        return new ResponseDTO<>(result.getETagValue(), result.getData());
    }

    @Operation(description = "Delete shape category, used token admin for access")
    @PreAuthorize("hasAuthority('ADMIN')")
    @DeleteMapping("/category/{categoryId}")
    public ResponseEntity<Void> deleteCategory(@PathVariable("categoryId") long id) throws ObjectNotFoundException {
        shapeService.deleteCategory(id);
        return ResponseEntity.ok().build();
    }

    @Operation(description = "Create shape category, used token admin for access")
    @PreAuthorize("hasAuthority('ADMIN')")
    @PostMapping("/category/save")
    public ResponseEntity<Long> saveCategory(@Valid @RequestBody ShapeCategoryDTO shapeCategoryDto) {
        return ResponseEntity.ok(shapeService.saveCategory(shapeCategoryDto).getId());
    }

    @PostMapping("/calc")
    public ResponseEntity<Double> calcAreaShape(@Valid @RequestBody ShapeDTO shapeDTO)
            throws ObjectNotFoundException, CalcAreaException {
        ShapeCategoryDTO category = this.shapeService.getCategory(shapeDTO.getCatId());

        if (validateAttribute(category, shapeDTO)) {
            Map<String, Double> mapAttribute = new HashMap<>();
            shapeDTO.getAttributes().forEach(mapAttribute::put);
            return ResponseEntity.ok(ShapeCalculatorUtils.calcArea(category.getAreaFormula(), mapAttribute));
        }

        return ResponseEntity.badRequest().build();
    }

    @PutMapping("/save")
    public ResponseDTO<ShapeDTO> saveShape(Authentication authentication, @Valid @RequestBody ShapeDTO shapeDTO)
            throws Exception {
        User user = (User) authentication.getPrincipal();
        ShapeCategoryDTO category = this.shapeService.getCategory(shapeDTO.getCatId());
        if (validateAttribute(category, shapeDTO)) {
            return new ResponseDTO<>(shapeService.saveShape(shapeDTO, user));
        }

        throw new Exception("");
    }

    @Parameter(in = ParameterIn.HEADER, description = "ETag", name = "If-None-Match")
    @GetMapping("/all")
    public ResponseDTO<List<ShapeDTO>> getAllShape() {
        ETagDTO<List<ShapeDTO>> result = shapeService.getAllShapesWithETag();
        return new ResponseDTO<>(result.getETagValue(), result.getData());
    }

    @Parameter(in = ParameterIn.HEADER, description = "ETag", name = "If-None-Match")
    @GetMapping("/{id}")
    public ResponseDTO<ShapeDTO> getShape(@PathVariable("id") long id)
            throws ObjectNotFoundException {
        ETagDTO<ShapeDTO> result = shapeService.getShapeByIdWithETag(id);
        return new ResponseDTO<>(result.getETagValue(), result.getData());
    }

    @DeleteMapping("/{id}")
    public ResponseEntity<Void> deleteShape(@PathVariable("id") long id) throws ObjectNotFoundException {
        shapeService.deleteShape(id);
        return ResponseEntity.ok().build();
    }

    private boolean validateAttribute(ShapeCategoryDTO category, ShapeDTO shape) {
        if (category.getAttributes().size() != shape.getAttributes().size()) {
            return false;
        }
        Set<String> fname1 = category.getAttributes();
        Set<String> fname2 = shape.getAttributes().keySet();
        return fname1.containsAll(fname2);
    }
}
