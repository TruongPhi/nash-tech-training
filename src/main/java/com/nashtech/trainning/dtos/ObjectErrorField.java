package com.nashtech.trainning.dtos;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.apache.commons.lang3.builder.ToStringBuilder;

import java.io.Serializable;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class ObjectErrorField implements Serializable {

    private static final long serialVersionUID = 6188071111305975235L;

    private String errorsCode;
    private String errorsDescription;
    private String errorsPrefix;
    private String errorField;

    @Override
    public String toString() {
        return ToStringBuilder.reflectionToString(this);
    }
}
