package com.nashtech.trainning.dtos;

import com.nashtech.trainning.commons.ErrorCode;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.apache.commons.lang3.builder.ToStringBuilder;

import java.io.Serializable;
import java.util.Map;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class ObjectError implements Serializable {

    private static final long serialVersionUID = 741087743510753649L;

    private String errorCode;
    private String errorDescription;
    private String errorTime;
    private Map<String, ObjectErrorField> errorFields;

    public ObjectError(ErrorCode errorCode) {
        this.errorCode = errorCode.value;
        this.errorDescription = errorCode.description;
    }

    public ObjectError(String errorCode, String errorDescription) {
        this.errorCode = errorCode;
        this.errorDescription = errorDescription;
    }

    @Override
    public String toString() {
        return ToStringBuilder.reflectionToString(this);
    }
}
