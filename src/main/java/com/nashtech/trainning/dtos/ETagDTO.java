package com.nashtech.trainning.dtos;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.io.Serializable;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class ETagDTO<T> implements Serializable {

    private static final long serialVersionUID = 4320143120630431355L;

    private String eTagValue;
    private T data;
}
