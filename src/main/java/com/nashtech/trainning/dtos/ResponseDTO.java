package com.nashtech.trainning.dtos;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.nashtech.trainning.commons.ResponseStatus;
import lombok.AllArgsConstructor;
import lombok.NoArgsConstructor;
import org.apache.commons.lang3.builder.ToStringBuilder;

import java.io.Serializable;

@NoArgsConstructor
@AllArgsConstructor
@JsonIgnoreProperties(ignoreUnknown = true)
public class ResponseDTO<T> implements Serializable {

    private static final long serialVersionUID = 6707605480733568787L;

    private ResponseStatus status = ResponseStatus.SUCCESS;
    private String eTag;
    private T response;
    private ObjectError objectError;

    public ResponseDTO(T response) {
        this.response = response;
    }

    public ResponseDTO(String eTag, T response) {
        this.response = response;
        this.eTag = eTag;
    }

    public ResponseDTO(ObjectError objectError) {
        this.status = ResponseStatus.FAIL;
        this.objectError = objectError;
    }

    public ResponseStatus getStatus() {
        return status;
    }

    public void setStatus(ResponseStatus status) {
        this.status = status;
    }

    @JsonIgnore
    public String getETag() {
        return eTag;
    }

    public void setETag(String eTag) {
        this.eTag = eTag;
    }

    public T getResponse() {
        return response;
    }

    public void setResponse(T response) {
        this.response = response;
    }

    public ObjectError getObjectError() {
        return objectError;
    }

    public void setObjectError(ObjectError objectError) {
        this.objectError = objectError;
    }

    @Override
    public String toString() {
        return ToStringBuilder.reflectionToString(this);
    }
}
