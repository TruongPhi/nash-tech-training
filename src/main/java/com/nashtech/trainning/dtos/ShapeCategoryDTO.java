package com.nashtech.trainning.dtos;

import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.Data;

import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import java.io.Serializable;
import java.util.Set;

@Data
public class ShapeCategoryDTO implements Serializable {

    private static final long serialVersionUID = 3618722830827717412L;

    private Long id;

    @NotNull
    @Size(min = 1, max = 50)
    private String name;

    @NotNull
    @Size(min = 1, max = 255)
    private String areaFormula;

    @JsonIgnore
    private Long version;

    @NotNull
    private Set<String> attributes;
}
