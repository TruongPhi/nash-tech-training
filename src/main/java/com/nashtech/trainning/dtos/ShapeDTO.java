package com.nashtech.trainning.dtos;

import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.Data;

import javax.validation.constraints.NotNull;
import java.io.Serializable;
import java.util.HashMap;
import java.util.Map;

@Data
public class ShapeDTO implements Serializable {

    private static final long serialVersionUID = 882169727055741302L;

    private Long id;

    @NotNull
    private Long catId;

    private String name;

    @JsonIgnore
    private Long version;

    private Map<String, Double> attributes = new HashMap<>();
}
