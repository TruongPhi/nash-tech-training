package com.nashtech.trainning.commons;

import com.fasterxml.jackson.annotation.JsonValue;
import org.apache.commons.lang3.StringUtils;

public enum ResponseStatus {

    SUCCESS, FAIL;

    @JsonValue
    @Override
    public String toString() {
        return StringUtils.capitalize(name().toLowerCase());
    }
}
