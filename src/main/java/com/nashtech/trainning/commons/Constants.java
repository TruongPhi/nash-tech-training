package com.nashtech.trainning.commons;

public class Constants {

    private Constants() {
        throw new IllegalStateException("Constants class");
    }

    //Cache message
    public static final String MAP_NAME_NOT_BLANK = "MapName not blank";
    public static final String KEY_NOT_BLANK = "key not blank";
}
