package com.nashtech.trainning.commons;

public enum ErrorCode {

    OBJECT_NOT_FOUND("E001", "Object not found"),
    PARAMS_INVALID("E002", "Request params invalid"),
    NOT_EMPTY("E003", "Not empty"),
    OBJECT_EXISTS("E004", "Object have exists"),
    UN_AUTHORIZED("E005", "Username or password invalid"),
    INVALID_REQUEST("E006", "Invalid request"),
    INVALID_CLIENT("E007", "Invalid client"),
    INVALID_GRANT("E008", "Invalid grant"),
    UNAUTHORIZED_CLIENT("E009", "Unauthorized client"),
    UNSUPPORTED_GRANT_TYPE("E010", "Unsupported grant type"),
    INVALID_SCOPE("E011", "Invalid scope"),
    INSUFFICIENT_SCOPE("E012", "Insufficient scope"),
    INVALID_TOKEN("E013", "Invalid token"),
    REDIRECT_URI_MISMATCH("E014", "Redirect uri mismatch"),
    UNSUPPORTED_RESPONSE_TYPE("E015", "Unsupported response type"),
    ACCESS_DENIED("E016", "Access denied"),
    EXCEPTION("E999", "Unknown exception");

    public final String value;
    public final String description;

    ErrorCode(String value, String description) {
        this.value = value;
        this.description = description;
    }

    public static ErrorCode fromValue(String value) {

        if (value == null) {
            return null;
        }
        for (ErrorCode errorsProperties : ErrorCode.values()) {
            if (errorsProperties.value.equals(value)) {
                return errorsProperties;
            }
        }

        return null;
    }
}
