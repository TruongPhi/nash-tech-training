## Shape API

```
Shape API Url: http://localhost:8080/api/shape
Swagger Url : http://localhost:8080/swagger-ui.html
```

## 1/ Accounts

```
Admin account: admin/password
User account : user/password
```

## 2/Profile

Config file location: {projectFolder}/src/main/resources/

This project has 3 properties files that correspond to 3 profiles, some properties only apply for default and uat 
(Example : mock verification code)

```
application.properties
application-uat.properties
application-prod.properties
```

 - **application.properties** : Default profile and setting 
 - **application-uat.properties** : QC profile, it will be used for QC environment and override setting in default 
 if properties are specified in file 
 - **application-prod.properties** : Production profile, it will be used for production environment 
 and override setting in default if properties are specified in file 
